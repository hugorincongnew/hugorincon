class ContactsController < ApplicationController
  
  def index
    
  end
  
  def new
    
  end
  
  def create
    @contact = Contact.new(data_params)
    @contact.save
    
    redirect_to "/"
  end
  
  def data_params
    params.permit(:name, :email, :subject, :message)
  end 
        
end
